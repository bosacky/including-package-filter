package sk.bosacky.log4j;

import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

public class IncludingPackageFilter extends Filter {

	public int decide(LoggingEvent aEvent) {
		for (int i = 0; i < packages.length; i++)
			if (aEvent.getLoggerName().startsWith(packages[i]))
				return 1;

		return -1;
	}

	public String getPackages() {
		String result = "";
		for (int i = 0; i < packages.length; i++)
			result = (new StringBuilder()).append(result).append(packages[i]).append(";").toString();

		return result;
	}

	public void setPackages(String packages) {
		this.packages = packages.split(";");
	}

	String packages[];
}
